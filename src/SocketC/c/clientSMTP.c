#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define SERV_TCP_PORT 1231

int sockfd;
char buffer[1024];

void initializeConnection(char Saddress[]) {
	struct sockaddr_in     	serv_addr ;
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("client: can't open stream socket") ;
		exit(-1);
		}

	serv_addr.sin_family        	= AF_INET ;
	serv_addr.sin_addr.s_addr   = inet_addr(Saddress);
	serv_addr.sin_port          	= htons(SERV_TCP_PORT) ;
		if (connect(sockfd, (struct sockaddr *) &serv_addr,
				sizeof(serv_addr)) != 0)  {
		perror("client: can't connect with server") ;
		exit(-1);
	}
}

//Probably not needed
void concatenate(char* in1, char* in2, char* out) {
	printf("%s\n", in1);
	printf("%s\n", in2);
	printf("%s\n", out);
	out = (char *) malloc(1 + strlen(in1)+ strlen(in2) );
	strcpy(out, in1);
	strcat(out, in2);
	printf("%s\n", in1);
	printf("%s\n", in2);
	printf("%s\n", out);
}

void printBuffer() {
  printf("Server->%s\n",buffer);
  int responseID = atoi(buffer);
  if (responseID!=220 && responseID!=250 && responseID!=354) {
    printf("Unexpected error\n");
    exit(-1);
  }
}

int receiveSMTP() {
	int n;
	if ((n=read(sockfd,buffer,1024))<0){
	 perror("ERROR: recv");
	 exit(-1);
 }
 return n;
}

void sendSMTP (char* textToSend) {
	int n;
	bzero(buffer, sizeof(buffer));
	textToSend.concat("\r\n");

	if ((n=send(sockfd,buffer, strlen(buffer),0))!=strlen(buffer)) {
	  perror("client: error in send");
	  exit(-1);
  }

	n = readBuffer();
	if (n < 0) {
		cout<<"ERROR reading from socket";
		exit(-1);
	}
	printBuffer();
}

int main() {
	int n;
	char Saddress[30];
	char* base, input;

	printf("Dirección IP del servidor (a.b.c.d) => ");
  gets(Saddress);

	initializeConnection(Saddress);

	printf("Stablished connection with fakeSMTP\n");

	bzero(buffer, sizeof(buffer));
	receiveSMTP(&buffer);
	printBuffer();

	bzero(buffer, sizeof(buffer));
	gets(buffer);
	printf("%s\n", buffer);
	sendSMTP(&buffer);

  bzero(buffer, sizeof(buffer));
	receiveSMTP(&buffer);

	printf("%s\n", buffer);
	printf("%s\n", buffer);


	close(sockfd);
}
