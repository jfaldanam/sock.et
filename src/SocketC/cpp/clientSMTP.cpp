#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
#include <cstring>
#include <unistd.h>
#include <iostream>
using namespace std;
#define PORT 1231

int sock;
char buffer[1024] = {0};

void initializeConnection(string Saddress) {
  struct sockaddr_in address;
  struct sockaddr_in serv_addr;
  //Initialize socket and connection
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      printf("\n Socket creation error \n");
      exit(-1);
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);



  // Convert IPv4 and IPv6 addresses from text to binary form
  if(inet_pton(AF_INET, Saddress.c_str(), &serv_addr.sin_addr)<=0) {
      printf("\nInvalid address/ Address not supported \n");
      exit(-1);
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
      printf("\nConnection Failed \n");
      exit(-1);
  }
}

void printBuffer() {
  cout<<"Server->"<<buffer;
  int responseID = atoi(buffer);
  if (responseID!=220 && responseID!=250 && responseID!=354) {
    cout << "Unexpected error";
    exit(-1);
  }
}

ssize_t readBuffer() {
  ssize_t res = read(sock, buffer, 1024);
  if (res < 0) {
      cout << "ERROR reading from socket";
      exit(-1);
  }
  return res;
}

void sendMsg(string sendText) {
  size_t n;
  bzero(buffer, sizeof(buffer));
  sendText += '\r';
  sendText += '\n';

  n = write(sock,sendText.c_str(),sendText.length());
  if (n < 0){
    cout<<"ERROR writing to socket\n";
    exit(-1);
  }

  n = readBuffer();
  if (n < 0) {
    cout<<"ERROR reading from socket";
    exit(-1);
  }
  printBuffer();
}

int main(int argc, char const *argv[]) {
  string textToSend, temp;
  string fromMail, toMail;
  cout<<"Type SMTP server address"<<endl;
  string Saddress = "";
  cin>>Saddress;

  initializeConnection(Saddress);

  readBuffer();
  printBuffer();

  cout<<"Insert FROM email\n";
  textToSend = "MAIL FROM:<";
  cin>>fromMail;
  textToSend.append(fromMail);
  textToSend.append(">");

  sendMsg(textToSend);

  cout<<"Insert their email\n";
  textToSend = "RCPT TO:<";
  cin>>toMail;
  textToSend.append(toMail);
  textToSend.append(">");

  sendMsg(textToSend);

  textToSend = "DATA";

  sendMsg(textToSend);


  textToSend = "MIME-Version: 1.0\r\n";
  textToSend.append("From: <");
  textToSend.append(fromMail);
  textToSend.append(">\r\n");
  textToSend.append("To: <");
  textToSend.append(toMail);
  textToSend.append(">\r\n");
  textToSend.append("Subject: ");
  cout<<"Type the subject of the mail"<<endl;
  cin.ignore();
  getline(cin, temp);
  textToSend.append(temp);
  textToSend.append("\r\n");
  textToSend.append("\r\n");
  cout<<"Type text to send, send FIN to finish"<<endl;
  do {
    getline(cin, temp);
    if (temp!="FIN") {
      textToSend.append(temp);
      textToSend.append("\r\n");
    }
  } while (temp!="FIN");
  textToSend.append("\r\n");
  textToSend.append(".");
  sendMsg(textToSend);

  cout<<"Email sent succesfully\n";
}
