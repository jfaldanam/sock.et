package SocketTCP;
import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        String inputKey, inputText, output;
        try {
            ServerSocket socket = new ServerSocket(port, 1);
            System.out.println("Server initialization at port: " + port);


            while (true) {
                System.out.println("Waiting for new client");
                Socket newSocket = socket.accept();
                try {
                    newSocket.setSoTimeout(40000);
                    BufferedReader in = new BufferedReader(new InputStreamReader(newSocket.getInputStream()));
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(newSocket.getOutputStream())),true);


                    out.println("Welcome to the criptographic service");
                    System.out.println("Connected to client: " + newSocket.getRemoteSocketAddress());

                    while (true) {
                        inputKey = in.readLine();
                        if (inputKey.equals("0")) {
                            System.out.println("Connection closed with client");
                            out.println("Connection closed correctly");
                            newSocket.close();
                            break;
                        }

                        inputText = in.readLine();
                        output = encrypt(inputText, Integer.parseInt(inputKey));
                        System.out.println(inputText + " translated to " + output + " with key " + inputKey);

                        out.println(output);

                     }

                } catch (SocketTimeoutException e) {
                    System.err.println("Connection closed: Read Time Out");
                    newSocket.close();
                    continue;
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String encrypt(String input, int key) {
        int numLetter = 'Z' - 'A' + 1;
        key = key % numLetter;
        String output = "";

        for (char c : input.toCharArray()) {
            if (Character.isAlphabetic(c)) {
                if ((Character.isUpperCase(c) && c + key > 'z') || (Character.isLowerCase(c) && c + key > 'z')) {
                    c = (char)(c - numLetter);
                }
                c = (char) (c + key);
            }

            output += c;
        }

        return output;
    }
}
