package SocketTCP;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) {
        String text, key;
        InetAddress server = null;
        int serverPort;

        if (args.length < 2) {
            System.out.println("Need to pass IP and port by parameters");
            return;
        }

        try {
            server = InetAddress.getByName(args[0]);
            serverPort = Integer.parseInt(args[1]);
            System.out.println("Trying to connect to server in: " + server.toString() + ":" + serverPort);


            Socket socket = new Socket(server, serverPort);




            BufferedReader inputKey = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);

            System.out.println("Connected to server: " + server +"\n" + in.readLine());

            while (true) {
                System.out.println("Introduce the key");
                key = inputKey.readLine();

                out.print(key + "\r\n");

                if (key.equals("0")) {
                    out.println("Connection closed");
                    break;
                }

                System.out.println("Introduce the text");
                text = inputKey.readLine();

                out.println(text);

                System.out.println("Server: " + in.readLine());
            }


            System.out.println("Disconnected to server: " + server +"\nStatus: " + in.readLine());

            inputKey.close();
            in.close();
            out.close();
            socket.close();
        } catch (UnknownHostException e) {
            System.out.println("Unknown: " + server);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
